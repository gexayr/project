<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::group(['prefix'=>'admin','middleware'=>'auth'], function (){
//    admin
    Route::get('/', function (){
        if (view()->exists('admin.index')) {
            $data = ['title'=>'admin panel'];

            return view('admin.index', $data);
//                return redirect('admin/pages');
        } else {
            $view = 'site.index';
        }
        return view($view, [
            'category' => "",
        ]);
    });

    Route::group(['prefix'=>'pages'], function (){

//        admin/pages
        Route::get('/',['uses'=>'PagesController@execute', 'as'=>'pages']);

        //        admin/pages/add
        Route::match(['get','post'],'/add',['uses'=>'PagesAddController@execute','as'=>'pagesAdd']);

        //        admin/pages/edit/2
        Route::match(['get','post','delete'],'/edit/{page}',['uses'=>'PagesEditController@execute','as'=>'pagesEdit']);

    });


    Route::group(['prefix'=>'services'], function (){

//        admin/services
        Route::get('/',['uses'=>'ServicesController@execute', 'as'=>'services']);

        //        admin/services/add
        Route::match(['get','post'],'/add',['uses'=>'ServicesAddController@execute','as'=>'servicesAdd']);

        //        admin/services/edit/2
        Route::match(['get','post','delete'],'/edit/{price}',['uses'=>'ServicesEditController@execute','as'=>'servicesEdit']);

    });


    Route::group(['prefix'=>'prices'], function (){

//        admin/prices
        Route::get('/',['uses'=>'PricesController@execute', 'as'=>'prices']);

        //        admin/prices/add
        Route::match(['get','post'],'/add',['uses'=>'PricesAddController@execute','as'=>'pricesAdd']);

        //        admin/prices/edit/2
        Route::match(['get','post','delete'],'/edit/{price}',['uses'=>'PricesEditController@execute','as'=>'pricesEdit']);

    });

});


Auth::routes();

Route::get('/services',['uses'=>'ServiceController@execute', 'as'=>'service']);
//Route::get('/calc',['uses'=>'ServiceController@execute', 'as'=>'service']);

Route::get('/regulation',['uses'=>'PagesController@regulation', 'as'=>'service']);
//Route::get('/calc',['uses'=>'ServiceController@execute', 'as'=>'service']);


Route::get('/contacts',['uses'=>'ContactController@execute', 'as'=>'contact']);

Route::get('/calc', ['uses'=>'CalcController@execute', 'as'=>'calc']);


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'web'], function (){
    Route::match(['get','post'],'/',['uses'=>'IndexController@execute','as'=>'home']);
    Route::get('/{alias}',['uses'=>'PageController@execute','as'=>'page']);

    Route::auth();
});



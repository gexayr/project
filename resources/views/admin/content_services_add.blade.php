<div class="wrapper container-fluid">

    <div class="content col-md-10 col-md-offset-1">

        {!! Form::open(['url'=>route('servicesAdd'), 'method'=>'POST', 'class'=>'form-horizontal']) !!}

        <div class="form-group">

            {!! Form::label('name','Name',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Input name']) !!}
            </div>

        </div>


        <div class="form-group">
            <div class="col-md-offset-2 col-md-8">
                {!! Form::button('Save',['class'=>'save btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>
    </div>
</div>

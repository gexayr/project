<div class="wrapper container-fluid">

    @foreach($services as $service)
        <?php $select[$service->id] = $service->name; ?>
    @endforeach

    <div class="content col-md-10 col-md-offset-1">

        {!! Form::open(['url'=>route('pricesEdit',array('price'=>$data['id'])), 'method'=>'POST', 'class'=>'form-horizontal']) !!}

        <div class="form-group">
            {!! Form::hidden('id',$data['id']) !!}
            {!! Form::label('name','Name',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('name',$data['name'],['class'=>'form-control','placeholder'=>'Input name']) !!}
            </div>

        </div>

        <div class="form-group">

            {!! Form::label('service_id','Service',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {{--{!! Form::text('text',$data['category_id'],['id'=>'editor','class'=>'form-control']) !!}--}}
                {{ Form::select('service_id', $select,  $data['service_id'], array('class'=>'form-control')) }}

            </div>

        </div>

        <div class="form-group">

            {!! Form::label('price','Price',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('price',$data['price'],['id'=>'editor','class'=>'form-control']) !!}
            </div>

        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-8">
                {!! Form::button('Save',['class'=>'save btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>
    </div>
</div>

<header>
    <div class="container header inner">
        <nav class="navbar">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="section-title">
                        <h2>{{ $title }}</h2>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right masthead-nav">
                        <li>
                            <a href="{{ route('pages') }}">
                                <h4>Страницы</h4>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('services') }}">
                                <h4>Услуги</h4>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('prices') }}">
                                <h4>Цены</h4>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
    </div>
</header>

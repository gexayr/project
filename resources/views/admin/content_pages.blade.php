<div class="content col-md-10 col-md-offset-1">
    @if($pages)
        <table class="table">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Link</th>
                <th>Text</th>
                <th>Date</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($pages as $ka => $page)
                    <tr>
                        <td>{{ $page->id }}</td>
                        <td>{!! Html::link(route('pagesEdit',['page'=>$page->id]),$page->name,['alt'=>$page->name]) !!}</td>
                        <td>{{ $page->alias }}</td>
                        <td><div>{!! str_limit($page->text,500)  !!}</div></td>
                        <td>{{ $page->created_at }}</td>
                        <td>

                            {!! Form::open(['url'=>route('pagesEdit',['page'=>$page->id]), 'method'=>'DELETE', 'class'=>'form-horizontal']) !!}

                                {{--{!! Form::hidden('_method','delete') !!}--}}
                            {{--<input  type="hidden" name="_method" value="DELETE">--}}
                            {{ method_field('DELETE') }}
                                {!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}

                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

        {!! Html::link(route('pagesAdd'),'Add',array('class'=>'btn btn-primary add-button')) !!}
</div>
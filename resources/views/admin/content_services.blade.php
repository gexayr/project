<div class="content col-md-10 col-md-offset-1">
    @if($services)
        <table class="table">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Date</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($services as $ka => $service)
                    <tr>
                        <td>{{ $service->id }}</td>
                        <td>{!! Html::link(route('servicesEdit',['service'=>$service->id]),$service->name,['alt'=>$service->name]) !!}</td>
                        <td>{{ $service->created_at }}</td>
                        <td>

                            {!! Form::open(['url'=>route('servicesEdit',['service'=>$service->id]), 'method'=>'DELETE', 'class'=>'form-horizontal']) !!}

                                {{--{!! Form::hidden('_method','delete') !!}--}}
                            {{--<input  type="hidden" name="_method" value="DELETE">--}}
                            {{ method_field('DELETE') }}
                                {!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}

                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

        {!! Html::link(route('servicesAdd'),'Add',array('class'=>'btn btn-primary add-button')) !!}
</div>
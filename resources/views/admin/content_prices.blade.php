<div class="content col-md-10 col-md-offset-1">
    @if($prices)
        <table class="table">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Service</th>
                <th>Price</th>
                <th>Date</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($prices as $ka => $price)
                    <tr>
                        <td>{{ $price->id }}</td>
                        <td>{!! Html::link(route('pricesEdit',['price'=>$price->id]),$price->name,['alt'=>$price->name]) !!}</td>
                        <td>{{ $price->service_id }}</td>
                        <td>{!! $price->price  !!}</td>
                        <td>{{ $price->created_at }}</td>
                        <td>

                            {!! Form::open(['url'=>route('pricesEdit',['price'=>$price->id]), 'method'=>'DELETE', 'class'=>'form-horizontal']) !!}

                                {{--{!! Form::hidden('_method','delete') !!}--}}
                            {{--<input  type="hidden" name="_method" value="DELETE">--}}
                            {{ method_field('DELETE') }}
                                {!! Form::button('Delete',['class'=>'btn btn-danger','type'=>'submit']) !!}

                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

        {!! Html::link(route('pricesAdd'),'Add',array('class'=>'btn btn-primary add-button')) !!}
</div>
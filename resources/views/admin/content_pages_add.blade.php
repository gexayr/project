<div class="wrapper container-fluid">

    <div class="content col-md-10 col-md-offset-1">

        {!! Form::open(['url'=>route('pagesAdd'), 'method'=>'POST', 'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}

        <div class="form-group">

            {!! Form::label('name','Name',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Input name']) !!}
            </div>

        </div>


        <div class="form-group">

            {!! Form::label('menu','Menu',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {{--{!! Form::text('menu',$data['menu'],['class'=>'form-control','placeholder'=>'Menu?']) !!}--}}
                {{ Form::checkbox('menu', 1,null,['class'=>'form-control checkbox']) }}
            </div>

        </div>


        <div class="form-group">

            {!! Form::label('alias','Link',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::text('alias',old('alias'),['class'=>'form-control','placeholder'=>'Input Link']) !!}
            </div>

        </div>


        <div class="form-group">

            {!! Form::label('text','Content',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::textarea('text',old('text'),['id'=>'editor','class'=>'form-control']) !!}
            </div>

        </div>
        <div class="form-group">

            {!! Form::label('images','Image',['class'=>'col-xs-2 control-label']) !!}
            <div class="col-xs-8">
                {!! Form::file('images') !!}
            </div>

        </div>


        <div class="form-group">
            <div class="col-md-offset-2 col-md-8">
                {!! Form::button('Save',['class'=>'save btn btn-primary','type'=>'submit']) !!}
            </div>
        </div>


        <script>
            CKEDITOR.replace('editor');
        </script>
    </div>
</div>

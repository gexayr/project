<div class="container1">
    <div class="row1">

        @if(isset($pages) && is_object($pages))

            <ul class="home_ul">
            @foreach($pages as $k=>$page)

                {{--<li><a href="{{ route('page', array('alias'=>$page->alias)) }}">{{ $page->name }}</a></li>--}}
            @endforeach
            </ul>
            <h1 class="hero-title">Полный комплекс услуг по ремонту квартир. Качественно и без лишних затрат!</h1>
{{--+++++++++++++++++++--}}

                    <div id="post-2" class="post-2 page type-page status-publish hentry">
                        <div>
                            <div class="elementor elementor-2">
                                <div class="elementor-inner">
                                    <div class="elementor-section-wrap">
                                        <section data-id="c22e20e"
                                                 class="elementor-element elementor-element-c22e20e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                                 data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div data-id="365510d"
                                                         class="elementor-element elementor-element-365510d elementor-column elementor-col-100 elementor-top-column"
                                                         data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div data-id="d2a1ce6"
                                                                     class="elementor-element elementor-element-d2a1ce6 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: justify;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Компания «Наша Бригада» выполнит ремонт квартир под ключ для жителей столицы и Подмосковья с учетом пожеланий заказчика. Мы работаем и с материалами клиента, и оказываем помощь в их приобретении по доступным ценам. Мечту преобразить свой дом или квартиру в Москве и других городах Московской области в стильный и уютный семейный очаг воплотят в реальность специалисты с опытом успешного выполнения проектов любого уровня сложности.</span>
                                                                            </p>
                                                                            <p style="text-align: justify;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Каждый из нас периодически сталкивается с традиционной проблемой – нужен хороший и качественный ремонт под ключ перед вселением. Мы предлагаем весь комплекс услуг по обустройству помещений любой сложности – косметический, капитальный и элитный варианты строительно-ремонтных мероприятий в частных домах, коттеджах, квартирах, офисных зданиях и гостиницах, ресторанах и торговых заведениях.</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="228d4d8"
                                                                     class="elementor-element elementor-element-228d4d8 elementor-widget elementor-widget-heading"
                                                                     data-element_type="heading.default">
                                                                    <div class="elementor-widget-container"><h2
                                                                                class="elementor-heading-title elementor-size-default">
                                                                            Популярные услуги по отделке и ремонту
                                                                            квартир «под ключ»</h2></div>
                                                                </div>
                                                                <div data-id="006256f"
                                                                     class="elementor-element elementor-element-006256f elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: justify;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Необходим комплекс работ по трансформации внутреннего пространства в настоящий оазис тепла и комфорта? Требуется заказать отделочные работы с гарантией профессионального выполнения и выполнения оговоренных сроков? Мы занимаемся ремонтом в квартирах 8 лет и способны сделать роскошным интерьер любого объекта. Воспользуйтесь возможностями лучшего предложения на столичном рынке! Выбирайте услуги мастеров с огромным опытом работы и высоким уровнем квалификации!</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="b985173"
                                                                         class="elementor-element elementor-element-b985173 elementor-section-boxed elementor-section-height-default elementor-section-height-min-height elementor-section-content-middle elementor-section elementor-inner-section"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="8a87ef0"
                                                                                 class="elementor-element elementor-element-8a87ef0 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-background-overlay"></div>
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="b69e687"
                                                                                             class="elementor-element elementor-element-b69e687 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-star"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <h3 class="elementor-icon-box-title">
                                                                                                            <span>Ремонт квартир</span>
                                                                                                        </h3>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Выполнение
                                                                                                            необходимых
                                                                                                            мероприятий
                                                                                                            по
                                                                                                            благоустройству
                                                                                                            всего
                                                                                                            объекта или
                                                                                                            его
                                                                                                            отдельных
                                                                                                            участков по
                                                                                                            согласованию
                                                                                                            с
                                                                                                            заказчиком.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="814319c"
                                                                                 class="elementor-element elementor-element-814319c elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-background-overlay"></div>
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="8477fe8"
                                                                                             class="elementor-element elementor-element-8477fe8 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-star"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <h3 class="elementor-icon-box-title">
                                                                                                            <span>Косметический ремонт</span>
                                                                                                        </h3>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Отличный
                                                                                                            выбор для
                                                                                                            тех
                                                                                                            клиентов,
                                                                                                            кто не
                                                                                                            обладает
                                                                                                            достаточными
                                                                                                            средствами и
                                                                                                            временем для
                                                                                                            капитальной
                                                                                                            реконструкции
                                                                                                            квартиры.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="5f53cac"
                                                                                 class="elementor-element elementor-element-5f53cac elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-background-overlay"></div>
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="d6e879a"
                                                                                             class="elementor-element elementor-element-d6e879a elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-star"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <h3 class="elementor-icon-box-title">
                                                                                                            <span>Ремонты с нуля</span>
                                                                                                        </h3>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Полный
                                                                                                            ремонт в
                                                                                                            старых домах
                                                                                                            и
                                                                                                            новостройках.
                                                                                                            Демонтаж,
                                                                                                            электрика,
                                                                                                            сантехника,
                                                                                                            малярные
                                                                                                            работы
                                                                                                            перепланировка.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="ef4a4fc"
                                                                     class="elementor-element elementor-element-ef4a4fc elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="0877800"
                                                                         class="elementor-element elementor-element-0877800 elementor-section-full_width elementor-section-height-default elementor-section-height-min-height elementor-section-content-middle elementor-section elementor-inner-section"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="b825ac7"
                                                                                 class="elementor-element elementor-element-b825ac7 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-background-overlay"></div>
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="7f5a8c6"
                                                                                             class="elementor-element elementor-element-7f5a8c6 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-star"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <h3 class="elementor-icon-box-title">
                                                                                                            <span>Новостройки</span>
                                                                                                        </h3>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            После
                                                                                                            покупки
                                                                                                            недвижимости
                                                                                                            почти всегда
                                                                                                            ощущается
                                                                                                            недостаток
                                                                                                            финансов.
                                                                                                            Идеальный
                                                                                                            вариант в
                                                                                                            подобном
                                                                                                            случае –
                                                                                                            косметическая
                                                                                                            отделка.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="631888c"
                                                                                 class="elementor-element elementor-element-631888c elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-background-overlay"></div>
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="4a9defb"
                                                                                             class="elementor-element elementor-element-4a9defb elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-star"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <h3 class="elementor-icon-box-title">
                                                                                                            <span>Ванные комнаты и туалеты</span>
                                                                                                        </h3>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Благоустройство
                                                                                                            с
                                                                                                            использованием
                                                                                                            качественных
                                                                                                            материалов
                                                                                                            для
                                                                                                            потолков,
                                                                                                            пола и стен,
                                                                                                            замена и
                                                                                                            монтаж
                                                                                                            сантехники и
                                                                                                            магистралей.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="cffa005"
                                                                                 class="elementor-element elementor-element-cffa005 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-background-overlay"></div>
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="92da8e2"
                                                                                             class="elementor-element elementor-element-92da8e2 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-star"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <h3 class="elementor-icon-box-title">
                                                                                                            <span>Мелкий бытовой ремонт</span>
                                                                                                        </h3>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            В штате есть
                                                                                                            специалисты,
                                                                                                            способные
                                                                                                            оказать вам
                                                                                                            помощь в
                                                                                                            незначительных
                                                                                                            по объему,
                                                                                                            но требующих
                                                                                                            затрат
                                                                                                            времени,
                                                                                                            бытовых
                                                                                                            мероприятиях.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="ed3d5cb"
                                                                     class="elementor-element elementor-element-ed3d5cb elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="58ae0e2"
                                                                     class="elementor-element elementor-element-58ae0e2 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: justify;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Большой перечень услуг, предоставляемых мастерами «Нашей Бригады», позволит решить любую возникшую проблему. Для успешного выполнения&nbsp;комплексного ремонта помещения под ключ&nbsp;ведется постоянный и скрупулезный мониторинг рынка с анализом интересов потребителей. Ответственный подход к интересам клиентов включает сотрудничество с надежными субподрядчиками.</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="2652553"
                                                                         class="elementor-element elementor-element-2652553 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="8c83437"
                                                                                 class="elementor-element elementor-element-8c83437 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="466df93"
                                                                                             class="elementor-element elementor-element-466df93 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p><strong><span
                                                                                                                    style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 18px;">Есть вопросы? Давайте встретимся и обсудим будущий ремонт</span></strong>
                                                                                                    </p>
                                                                                                    <p style="text-align: justify;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #000000;">Приняли решение начинать&nbsp;ремонт в квартире под ключ, но не знаете с чего начать? Компетентные специалисты будут рады предоставить полную информацию и консультации по всем нюансам, которыми сопровождается&nbsp;реставрация в квартирах&nbsp;и&nbsp;отделки помещения&nbsp;в любом состоянии.</span>
                                                                                                    </p></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="5045c07"
                                                                                 class="elementor-element elementor-element-5045c07 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="811db08"
                                                                                             class="elementor-element elementor-element-811db08 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p style="text-align: center;">
                                                                                                        <span style="color: #ff6600; font-size: 20px;"><span style="font-family: arial, helvetica, sans-serif; color: #000000;">+7 (985) 127 8559</span></span>
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-id="69bbbd5"
                                                                                             class="elementor-element elementor-element-69bbbd5 elementor-align-center elementor-widget elementor-widget-button"
                                                                                             data-element_type="button.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-button-wrapper">
                                                                                                    <a href="/contacts"
                                                                                                       class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                                       target="_blank"
                                                                                                       rel="nofollow"
                                                                                                       role="button">
                                                                                                        <span class="elementor-button-content-wrapper"> <span
                                                                                                                    class="elementor-button-icon elementor-align-icon-left"> <i
                                                                                                                        class="fa fa-comments-o"
                                                                                                                        aria-hidden="true"></i> </span> <span
                                                                                                                    class="elementor-button-text">Заказать встречу со специалистами</span> </span>
                                                                                                    </a></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="bdb866b"
                                                                     class="elementor-element elementor-element-bdb866b elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="0ad5b90"
                                                                     class="elementor-element elementor-element-0ad5b90 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; font-size: 20px; color: #000000;">«Наша Бригада» – идеальное сочетание профессионализма персонала и большого опыта</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="4eda6f9"
                                                                         class="elementor-element elementor-element-4eda6f9 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="8ca8a93"
                                                                                 class="elementor-element elementor-element-8ca8a93 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="8554ed4"
                                                                                             class="elementor-element elementor-element-8554ed4 elementor-aspect-ratio-169 elementor-widget elementor-widget-video"
                                                                                             data-settings="{&quot;aspect_ratio&quot;:&quot;169&quot;}"
                                                                                             data-element_type="video.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
                                                                                                    <iframe src="https://www.youtube.com/embed/KXYKlyFpg1U?feature=oembed&amp;autoplay=0&amp;rel=0&amp;controls=1&amp;showinfo=0&amp;mute=0&amp;wmode=opaque"
                                                                                                            allowfullscreen=""></iframe>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="57c1082"
                                                                                 class="elementor-element elementor-element-57c1082 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="035c72b"
                                                                                             class="elementor-element elementor-element-035c72b elementor-align-left elementor-widget elementor-widget-icon-list"
                                                                                             data-element_type="icon-list.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <ul class="elementor-icon-list-items">
                                                                                                    <li class="elementor-icon-list-item">
                                                                                                        <span class="elementor-icon-list-icon"> <i
                                                                                                                    class="fa fa-arrow-circle-o-right"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                        <span class="elementor-icon-list-text">Cервис достойного уровня избавит заказчиков от забот и проблем, связанных с реконструкцией жилого пространства. Созданы все условия для максимально удобного и эффективного обслуживания.</span>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-id="fd9af65"
                                                                                             class="elementor-element elementor-element-fd9af65 elementor-align-left elementor-widget elementor-widget-icon-list"
                                                                                             data-element_type="icon-list.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <ul class="elementor-icon-list-items">
                                                                                                    <li class="elementor-icon-list-item">
                                                                                                        <span class="elementor-icon-list-icon"> <i
                                                                                                                    class="fa fa-arrow-circle-o-right"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                        <span class="elementor-icon-list-text">Квалификация мастеров позволяет успешно справляться с проектами различной сложности. Можно заказать простой косметический вариант реставрации помещения и наиболее сложное строительное решение.</span>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-id="1e4929c"
                                                                                             class="elementor-element elementor-element-1e4929c elementor-align-left elementor-widget elementor-widget-icon-list"
                                                                                             data-element_type="icon-list.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <ul class="elementor-icon-list-items">
                                                                                                    <li class="elementor-icon-list-item">
                                                                                                        <span class="elementor-icon-list-icon"> <i
                                                                                                                    class="fa fa-arrow-circle-o-right"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                        <span class="elementor-icon-list-text">Один из главных приоритетов – экономичность сметы, в которой присутствуют только необходимые расходные материалы и работы. Наши менеджеры помогут подобрать оптимальный для клиента вариант.</span>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="b3bc545"
                                                                     class="elementor-element elementor-element-b3bc545 elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="b4cffc4"
                                                                     class="elementor-element elementor-element-b4cffc4 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: justify;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Если вам требуется действительно&nbsp;красивый&nbsp;и очень&nbsp;качественный ремонт, обращайтесь к надежному поставщику услуг с проверенной репутацией. В самые сжатые&nbsp;сроки&nbsp;на объекте в любом состоянии выполнение&nbsp;ремонта квартиры под ключ&nbsp;преобразит внутреннее пространство, сделает его уютным уголком для отдыха, предметом вашей гордости перед гостями и знакомыми.</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="8188c99"
                                                                     class="elementor-element elementor-element-8188c99 elementor-align-center elementor-widget elementor-widget-icon-list"
                                                                     data-element_type="icon-list.default">
                                                                    <div class="elementor-widget-container">
                                                                        <ul class="elementor-icon-list-items">
                                                                            <li class="elementor-icon-list-item"><span
                                                                                        class="elementor-icon-list-icon"> <i
                                                                                            class="fa fa-arrow-circle-down"
                                                                                            aria-hidden="true"></i> </span>
                                                                                <span class="elementor-icon-list-text">6 основных причин, почему стоит выбрать нас</span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div data-id="b4f232b"
                                                                     class="elementor-element elementor-element-b4f232b elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Полностью&nbsp;удовлетворить запросы наиболее требовательных клиентов сможет&nbsp;профессиональный&nbsp;подход к поставленным задачам.&nbsp;Ремонт квартир&nbsp;по высшему разряду обусловлен следующими приоритетами в деятельности компании:</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="d72d859"
                                                                         class="elementor-element elementor-element-d72d859 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="3d0fe40"
                                                                                 class="elementor-element elementor-element-3d0fe40 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="f2870a0"
                                                                                             class="elementor-element elementor-element-f2870a0 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-check-square-o"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Причина №1</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Работа по
                                                                                                            договору. В
                                                                                                            подписанном
                                                                                                            сторонами
                                                                                                            официальном
                                                                                                            документе
                                                                                                            предусмотрены
                                                                                                            все
                                                                                                            обязательства
                                                                                                            и
                                                                                                            потенциальные
                                                                                                            риски.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="6b08f2b"
                                                                                 class="elementor-element elementor-element-6b08f2b elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="e695184"
                                                                                             class="elementor-element elementor-element-e695184 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-check-square-o"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Причина №2</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Расширенная
                                                                                                            гарантия. В
                                                                                                            договоре
                                                                                                            сроком на 1
                                                                                                            год даются
                                                                                                            гарантийные
                                                                                                            обязательства
                                                                                                            на все виды
                                                                                                            выполненных
                                                                                                            работ.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="c8599b1"
                                                                                 class="elementor-element elementor-element-c8599b1 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="9f71072"
                                                                                             class="elementor-element elementor-element-9f71072 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-check-square-o"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Причина №3</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Прозрачные
                                                                                                            цены. Вы
                                                                                                            платите
                                                                                                            только
                                                                                                            суммы,
                                                                                                            указанные в
                                                                                                            смете.
                                                                                                            Отсутствуют
                                                                                                            переплаты и
                                                                                                            замаскированные
                                                                                                            пункты.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <section data-id="44d1d9c"
                                                                         class="elementor-element elementor-element-44d1d9c elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="91dd725"
                                                                                 class="elementor-element elementor-element-91dd725 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="dce042c"
                                                                                             class="elementor-element elementor-element-dce042c elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-check-square-o"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Причина №4</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Соблюдение
                                                                                                            сроков.
                                                                                                            Стопроцентная
                                                                                                            гарантия
                                                                                                            выполнения
                                                                                                            всего объема
                                                                                                            работ в
                                                                                                            оговоренные
                                                                                                            и
                                                                                                            документально
                                                                                                            утвержденные
                                                                                                            сроки.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="a023f8e"
                                                                                 class="elementor-element elementor-element-a023f8e elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="52f8656"
                                                                                             class="elementor-element elementor-element-52f8656 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-check-square-o"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Причина №5</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Скидки на
                                                                                                            материалы.
                                                                                                            Выбор
                                                                                                            надежных
                                                                                                            поставщиков
                                                                                                            позволяет
                                                                                                            нам
                                                                                                            предоставить
                                                                                                            скидки и
                                                                                                            самые
                                                                                                            выгодные
                                                                                                            условия
                                                                                                            покупки.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="144892c"
                                                                                 class="elementor-element elementor-element-144892c elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="fa9bd45"
                                                                                             class="elementor-element elementor-element-fa9bd45 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-check-square-o"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Причина №6</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Все
                                                                                                            консультации
                                                                                                            по вопросам
                                                                                                            сотрудничества,
                                                                                                            выезд
                                                                                                            специалиста
                                                                                                            и
                                                                                                            составление
                                                                                                            подробной
                                                                                                            сметы
                                                                                                            оказываются
                                                                                                            бесплатно.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="2e45321"
                                                                     class="elementor-element elementor-element-2e45321 elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="a9f5638"
                                                                     class="elementor-element elementor-element-a9f5638 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: justify;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Наш штат укомплектован мастерами высокого уровня квалификации. Постоянный контроль на всех этапах производственного процесса способствует тому, что все&nbsp;работы под ключ&nbsp;будут выполнены с максимальной экономией средств, без каких-либо задержек и переделок. Залог отличного конечного результата – применение современных технологий и высококлассного инструмента!</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="442a68d"
                                                                     class="elementor-element elementor-element-442a68d elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <span style="font-family: arial, helvetica, sans-serif; font-size: 20px; color: #000000;">В чем выгода заказчика</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section data-id="a8d2f3d"
                                                 class="elementor-element elementor-element-a8d2f3d elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                                 data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                 data-element_type="section" style="width: 1530px; left: 0px;">
                                            <div class="elementor-background-overlay"></div>
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div data-id="ac5fc43"
                                                         class="elementor-element elementor-element-ac5fc43 elementor-column elementor-col-100 elementor-top-column"
                                                         data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <section data-id="c840c06"
                                                                         class="elementor-element elementor-element-c840c06 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-background-overlay"></div>
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="2e1237e"
                                                                                 class="elementor-element elementor-element-2e1237e elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="701000d"
                                                                                             class="elementor-element elementor-element-701000d elementor-widget elementor-widget-image"
                                                                                             data-element_type="image.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-image">
                                                                                                    <img width="300"
                                                                                                         height="200"
                                                                                                         src="//i0.wp.com/nashabrigada.ru/wp-content/uploads/2017/11/sotrudnichestvo.png?fit=300%2C200&amp;ssl=1"
                                                                                                         class="attachment-large size-large"
                                                                                                         alt=""></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="6f68fde"
                                                                                 class="elementor-element elementor-element-6f68fde elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="beb07f7"
                                                                                             class="elementor-element elementor-element-beb07f7 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <ul>
                                                                                                        <li style="text-align: left;">
                                                                                                            <span style="font-family: arial, helvetica, sans-serif; color: #000000;">Вы&nbsp;переживаете, что конечная&nbsp;стоимость ремонтных работ&nbsp;может отличаться от обещанной вначале?</span>
                                                                                                        </li>
                                                                                                        <li style="text-align: left;">
                                                                                                            <span style="font-family: arial, helvetica, sans-serif; color: #000000;">Обещаем –&nbsp;не переживайте! <strong>Цена ремонта квартиры&nbsp;всегда соответствует сметной и не меняется!</strong></span>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-id="6f33214"
                                                                                             class="elementor-element elementor-element-6f33214 elementor-align-center elementor-widget elementor-widget-button"
                                                                                             data-element_type="button.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-button-wrapper">
                                                                                                    <a href="/contacts"
                                                                                                       class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                                       target="_blank"
                                                                                                       rel="nofollow"
                                                                                                       role="button">
                                                                                                        <span class="elementor-button-content-wrapper"> <span
                                                                                                                    class="elementor-button-icon elementor-align-icon-left"> <i
                                                                                                                        class="fa fa-file-text-o"
                                                                                                                        aria-hidden="true"></i> </span> <span
                                                                                                                    class="elementor-button-text">Заказать профессиональный расчет сметы</span> </span>
                                                                                                    </a></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="c7a336b"
                                                                     class="elementor-element elementor-element-c7a336b elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p>
                                                                                <span style="font-family: arial, helvetica, sans-serif; color: #000000;">При расчетах&nbsp;всегда&nbsp;учитываются следующие моменты:</span>
                                                                            </p>
                                                                            <ul>
                                                                                <li style="text-align: left;"><span
                                                                                            style="font-family: arial, helvetica, sans-serif; color: #000000;">заказчик не должен испытывать трудностей при чтении сметы, из которой легко представить все нюансы работы;</span>
                                                                                </li>
                                                                                <li style="text-align: left;"><span
                                                                                            style="font-family: arial, helvetica, sans-serif; color: #000000;">документ должен подробно информировать обо всех предстоящих мероприятиях и окончательной сумме затрат;</span>
                                                                                </li>
                                                                                <li style="text-align: left;"><span
                                                                                            style="font-family: arial, helvetica, sans-serif; color: #000000;">при желании заказчика в смету удобно внести доработки и изменения, чтобы на этапе обсуждения предусмотреть все детали.</span>
                                                                                </li>
                                                                            </ul>
                                                                            <p>
                                                                                <span style="font-family: arial, helvetica, sans-serif; color: #000000;">Важный момент – предоставление при необходимости нескольких вариантов сметного документа.</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section data-id="32d6999"
                                                 class="elementor-element elementor-element-32d6999 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                                 data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div data-id="204d9d3"
                                                         class="elementor-element elementor-element-204d9d3 elementor-column elementor-col-100 elementor-top-column"
                                                         data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div data-id="53de448"
                                                                     class="elementor-element elementor-element-53de448 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <blockquote><p style="text-align: left;">
                                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000;"><i>Во главу всей своей деятельности «Наша Бригада» всегда ставит дружелюбные и доверительные отношения с клиентами. Помочь наладить конструктивный диалог и расположить заказчика к себе можно только предоставлением убедительных гарантий.</i></span>
                                                                                </p></blockquote>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="d0636f8"
                                                                         class="elementor-element elementor-element-d0636f8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="808a24e"
                                                                                 class="elementor-element elementor-element-808a24e elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="af1cc13"
                                                                                             class="elementor-element elementor-element-af1cc13 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 20px;">Как обманывают заказчиков при ремонте?</span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="4acbaab"
                                                                                 class="elementor-element elementor-element-4acbaab elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap">
                                                                                    <div class="elementor-widget-wrap"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <section data-id="dd2e784"
                                                                         class="elementor-element elementor-element-dd2e784 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="19cbebd"
                                                                                 class="elementor-element elementor-element-19cbebd elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="ecbe0a9"
                                                                                             class="elementor-element elementor-element-ecbe0a9 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p style="text-align: justify;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ffffff;">Перечислить все ухищрения, на которые способны недобросовестные мастера с недостаточной квалификацией, очень трудно. В ход идут такие приемы, как необоснованное снижение цены, предложение взяться за работу без достаточного опыта, элементарный обман.</span>
                                                                                                    </p>
                                                                                                    <p style="text-align: justify;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ffffff;">А потребитель уплатит в итоге значительно больше, постоянно обращаясь с просьбами переделать брак. Подобные «горе-специалисты» будут пользоваться безысходностью заказчика и постоянно вытягивать из него новые порции денег.</span>
                                                                                                    </p>
                                                                                                    <p style="text-align: justify;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ffffff;">Если отказаться от столь своеобразного сервиса, то привести подобные&nbsp;«ремонты»&nbsp;в порядок становится огромной проблемой. Потратить придется немало финансов, нервных клеток и времени.</span>
                                                                                                    </p></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="edb1f48"
                                                                                 class="elementor-element elementor-element-edb1f48 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="abcc1fa"
                                                                                             class="elementor-element elementor-element-abcc1fa animated animated-slow elementor-invisible elementor-widget elementor-widget-image"
                                                                                             data-settings="{&quot;_animation&quot;:&quot;shake&quot;}"
                                                                                             data-element_type="image.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-image">
                                                                                                    <img width="350"
                                                                                                         height="262"
                                                                                                         src="//i1.wp.com/nashabrigada.ru/wp-content/uploads/2018/03/ne-stani-obmanutnim.jpg?fit=350%2C262&amp;ssl=1"
                                                                                                         class="attachment-large size-large"
                                                                                                         alt=""></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-id="8f7c0e6"
                                                                                             class="elementor-element elementor-element-8f7c0e6 elementor-align-center elementor-widget elementor-widget-icon-list"
                                                                                             data-element_type="icon-list.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <ul class="elementor-icon-list-items">
                                                                                                    <li class="elementor-icon-list-item">
                                                                                                        <span class="elementor-icon-list-icon"> <i
                                                                                                                    class="fa fa-arrow-up"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                        <span class="elementor-icon-list-text">Жулик</span>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="111b6c1"
                                                                     class="elementor-element elementor-element-111b6c1 elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="9f9b742"
                                                                     class="elementor-element elementor-element-9f9b742 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <blockquote><p style="text-align: left;">
                                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000;"><i>Основная миссия, которой руководствуется наш коллектив – это избавить вас от хлопот, связанных реконструкцией помещения. Пусть все заботы возьмут на себя профессионалы, а вы будете расходовать ресурс времени на близких и родных людей!</i></span>
                                                                                </p></blockquote>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="7c76fb7"
                                                                     class="elementor-element elementor-element-7c76fb7 elementor-widget elementor-widget-heading"
                                                                     data-element_type="heading.default">
                                                                    <div class="elementor-widget-container"><h2
                                                                                class="elementor-heading-title elementor-size-default">
                                                                            Как строится наша работа с заказчиком</h2>
                                                                    </div>
                                                                </div>
                                                                <div data-id="9eb1224"
                                                                     class="elementor-element elementor-element-9eb1224 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: justify;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Специалистами&nbsp;«Нашей Бригады» будут выполнены все мероприятия по&nbsp;ремонту&nbsp;в домах и&nbsp;квартирах. Заказать комплексную&nbsp;реконструкцию у надежного поставщика услуг – это&nbsp;идеальный выбор&nbsp;для объекта любой&nbsp;планировки.&nbsp;Понятная схема сотрудничества состоит из нескольких простых шагов.</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="4e01b84"
                                                                         class="elementor-element elementor-element-4e01b84 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="26bcb84"
                                                                                 class="elementor-element elementor-element-26bcb84 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="9d17ccb"
                                                                                             class="elementor-element elementor-element-9d17ccb elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-arrow-circle-o-down"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>1-й этап</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            ЗАЯВКА.
                                                                                                            Звонок по
                                                                                                            номеру
                                                                                                            контактного
                                                                                                            телефона или
                                                                                                            заполнение
                                                                                                            формы заявки
                                                                                                            на
                                                                                                            сайте.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="838806f"
                                                                                 class="elementor-element elementor-element-838806f elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="60ceee0"
                                                                                             class="elementor-element elementor-element-60ceee0 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-arrow-circle-o-down"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>2-й этап</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            ЗАМЕР. В
                                                                                                            день
                                                                                                            обращения
                                                                                                            инженер-сметчик
                                                                                                            приедет на
                                                                                                            объект для
                                                                                                            осмотра и
                                                                                                            замеров.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="25c93be"
                                                                                 class="elementor-element elementor-element-25c93be elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="2455e42"
                                                                                             class="elementor-element elementor-element-2455e42 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-arrow-circle-o-down"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>3-й этап</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            СМЕТА. На
                                                                                                            все виды
                                                                                                            работ и
                                                                                                            материалы
                                                                                                            подготавливается
                                                                                                            подробная
                                                                                                            документация.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <section data-id="e150b0f"
                                                                         class="elementor-element elementor-element-e150b0f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="f318630"
                                                                                 class="elementor-element elementor-element-f318630 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="7610f73"
                                                                                             class="elementor-element elementor-element-7610f73 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-arrow-circle-o-down"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>4-й этап</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            ДОГОВОР.
                                                                                                            Обсуждается
                                                                                                            и
                                                                                                            подписывается
                                                                                                            договор,
                                                                                                            после
                                                                                                            которого
                                                                                                            начинается
                                                                                                            работа.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="bfa8057"
                                                                                 class="elementor-element elementor-element-bfa8057 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="33f6bd2"
                                                                                             class="elementor-element elementor-element-33f6bd2 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-arrow-circle-o-down"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>5-й этап</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            РЕМОНТ.
                                                                                                            Выполнение
                                                                                                            всех
                                                                                                            запланированных
                                                                                                            мероприятий
                                                                                                            и сдача
                                                                                                            вашего
                                                                                                            объекта под
                                                                                                            ключ.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="bc90cb7"
                                                                                 class="elementor-element elementor-element-bc90cb7 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="972a7a2"
                                                                                             class="elementor-element elementor-element-972a7a2 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-arrow-circle-o-down"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>6-й этап</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            ЗАВЕРШЕНИЕ.
                                                                                                            Сдача жилья
                                                                                                            заказчику в
                                                                                                            эксплуатацию
                                                                                                            в полностью
                                                                                                            рабочем
                                                                                                            состоянии.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="2753a6d"
                                                                     class="elementor-element elementor-element-2753a6d elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="e52e5ac"
                                                                         class="elementor-element elementor-element-e52e5ac elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="b8bca1a"
                                                                                 class="elementor-element elementor-element-b8bca1a elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="60bcaf2"
                                                                                             class="elementor-element elementor-element-60bcaf2 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p style="text-align: justify;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 18px;"><b>Узнайте, во сколько обойдется ваш ремонт</b></span>
                                                                                                    </p>
                                                                                                    <p style="text-align: justify;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #000000;">Все наши консультации, выполнение замеров и расчет подробно составленной сметы предоставляются абсолютно бесплатно.</span>
                                                                                                    </p></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="4efacb3"
                                                                                 class="elementor-element elementor-element-4efacb3 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="d78194e"
                                                                                             class="elementor-element elementor-element-d78194e elementor-align-center elementor-widget elementor-widget-button"
                                                                                             data-element_type="button.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-button-wrapper">
                                                                                                    <a href="/contacts"
                                                                                                       class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                                       target="_blank"
                                                                                                       rel="nofollow"
                                                                                                       role="button">
                                                                                                        <span class="elementor-button-content-wrapper"> <span
                                                                                                                    class="elementor-button-icon elementor-align-icon-left"> <i
                                                                                                                        class="fa fa-sign-out"
                                                                                                                        aria-hidden="true"></i> </span> <span
                                                                                                                    class="elementor-button-text">Заказать замер</span> </span>
                                                                                                    </a></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="4138e5e"
                                                                     class="elementor-element elementor-element-4138e5e elementor-widget elementor-widget-heading"
                                                                     data-element_type="heading.default">
                                                                    <div class="elementor-widget-container"><h2
                                                                                class="elementor-heading-title elementor-size-default">
                                                                            Цены на ремонт квартиры за 1м2 – 2018
                                                                            год</h2></div>
                                                                </div>
                                                                <div data-id="1b5ddc8"
                                                                     class="elementor-element elementor-element-1b5ddc8 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Тарифы за квадратный метр «от» и «до» указаны под ключ по площади пола и зависят от выбранного вами перечня услуг, сложности&nbsp;отдельно взятого объекта, класса расходных материалов. Иногда на стоимость влияет срочность выполнения заказа.</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section data-id="0a6d45e"
                                                 class="elementor-element elementor-element-0a6d45e elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                                 data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                                 data-element_type="section" style="width: 1530px; left: 0px;">
                                            <div class="elementor-background-overlay"></div>
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div data-id="6618024"
                                                         class="elementor-element elementor-element-6618024 elementor-column elementor-col-50 elementor-top-column"
                                                         data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div data-id="8d51356"
                                                                     class="elementor-element elementor-element-8d51356 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <table>
                                                                                <tbody>
                                                                                <tr style="background-color: #0ba5bd;">
                                                                                    <td style="text-align: left;"><span
                                                                                                style="color: #ffffff; font-family: arial, helvetica, sans-serif;">Виды ремонтов под ключ</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ffffff; font-family: arial, helvetica, sans-serif;">Цена руб./м2</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="color: #000000; font-family: arial, helvetica, sans-serif;">Самый экономичный вид ремонта</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ff6600; font-family: arial, helvetica, sans-serif;">от 1700 до 2900</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="font-family: arial, helvetica, sans-serif; color: #000000;">Косметический вариант</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ff6600; font-family: arial, helvetica, sans-serif;">от 2900 до 5700</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="font-family: arial, helvetica, sans-serif; color: #000000;">Полная реконструкция квартиры</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ff6600; font-family: arial, helvetica, sans-serif;">от 5700 до 8990</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="font-family: arial, helvetica, sans-serif; color: #000000;">Евроремонт</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ff6600; font-family: arial, helvetica, sans-serif;">от 8990 до 12000</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="font-family: arial, helvetica, sans-serif; color: #000000;">Ванные комнаты (весь комплекс работ)</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ff6600; font-family: arial, helvetica, sans-serif;">от 40 000</span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-id="6154674"
                                                         class="elementor-element elementor-element-6154674 elementor-column elementor-col-50 elementor-top-column"
                                                         data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div data-id="72c1573"
                                                                     class="elementor-element elementor-element-72c1573 elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <table>
                                                                                <tbody>
                                                                                <tr style="background-color: #0ba5bd;">
                                                                                    <td>
                                                                                        <span style="color: #ffffff; font-family: arial, helvetica, sans-serif;">Отделочные работы</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span style="color: #ffffff; font-family: arial, helvetica, sans-serif;">Цена руб./м2</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="color: #000000; font-family: arial, helvetica, sans-serif;">Штукатурка стен и потолков</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ff6600; font-family: arial, helvetica, sans-serif;">от 650</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="color: #000000; font-family: arial, helvetica, sans-serif;">Конструкций из гипсокартона</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="color: #ff6600; font-family: arial, helvetica, sans-serif;">от 700</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="font-family: arial, helvetica, sans-serif; color: #000000;">Укладка напольных покрытий</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ff6600;">от 250</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="font-family: arial, helvetica, sans-serif; color: #000000;">Малярные работы</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ff6600;">от 300</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left;"><span
                                                                                                style="font-family: arial, helvetica, sans-serif; color: #000000;">Натяжные потолки</span>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ff6600;">от 550</span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section data-id="27a2f54"
                                                 class="elementor-element elementor-element-27a2f54 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                                 data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div data-id="703719f"
                                                         class="elementor-element elementor-element-703719f elementor-column elementor-col-100 elementor-top-column"
                                                         data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <section data-id="8ed6432"
                                                                         class="elementor-element elementor-element-8ed6432 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="a60da17"
                                                                                 class="elementor-element elementor-element-a60da17 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="d2ed570"
                                                                                             class="elementor-element elementor-element-d2ed570 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p>
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #000000;">Что делать, если вы не нашли нужный тариф? Откройте полный прайс-лист, нажав на расположенную ниже кнопку. Откроется обзор с расценками из расчета за квадратный метр по любому виду работ.</span>
                                                                                                    </p></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="f287a0b"
                                                                                 class="elementor-element elementor-element-f287a0b elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="a080597"
                                                                                             class="elementor-element elementor-element-a080597 elementor-align-center elementor-widget elementor-widget-button"
                                                                                             data-element_type="button.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-button-wrapper">
                                                                                                    <a href="/services"
                                                                                                       class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                                                                       role="button">
                                                                                                        <span class="elementor-button-content-wrapper"> <span
                                                                                                                    class="elementor-button-text">Цены на 2018 год</span> </span>
                                                                                                    </a></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="ea494e6"
                                                                     class="elementor-element elementor-element-ea494e6 elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="d5fc58d"
                                                                     class="elementor-element elementor-element-d5fc58d elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Способ оплаты – выберите удобный для вас вариант</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="998494f"
                                                                         class="elementor-element elementor-element-998494f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="b1be41d"
                                                                                 class="elementor-element elementor-element-b1be41d elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="d325062"
                                                                                             class="elementor-element elementor-element-d325062 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-bank"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Безналичная оплата</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Операция по
                                                                                                            выставленному
                                                                                                            счету с
                                                                                                            учетом
                                                                                                            НДС.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="6b5f697"
                                                                                 class="elementor-element elementor-element-6b5f697 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="e6b95f9"
                                                                                             class="elementor-element elementor-element-e6b95f9 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-rub"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Наличный расчет</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Оплачиваете
                                                                                                            услуги
                                                                                                            наличными
                                                                                                            деньгами.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="67cbf87"
                                                                                 class="elementor-element elementor-element-67cbf87 elementor-column elementor-col-33 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="92bf0e0"
                                                                                             class="elementor-element elementor-element-92bf0e0 elementor-view-framed elementor-shape-square elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                                             data-element_type="icon-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-icon-box-wrapper">
                                                                                                    <div class="elementor-icon-box-icon">
                                                                                                        <span class="elementor-icon elementor-animation-"> <i
                                                                                                                    class="fa fa-cc-visa"
                                                                                                                    aria-hidden="true"></i> </span>
                                                                                                    </div>
                                                                                                    <div class="elementor-icon-box-content">
                                                                                                        <p class="elementor-icon-box-title">
                                                                                                            <span>Расчет картой</span>
                                                                                                        </p>
                                                                                                        <p class="elementor-icon-box-description">
                                                                                                            Можно
                                                                                                            воспользоваться
                                                                                                            банковской
                                                                                                            картой</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="6817042"
                                                                     class="elementor-element elementor-element-6817042 elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section data-id="50b90eb"
                                                 class="elementor-element elementor-element-50b90eb elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                                 data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}"
                                                 data-element_type="section" style="width: 1530px; left: 0px;">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div data-id="32be4e0"
                                                         class="elementor-element elementor-element-32be4e0 elementor-column elementor-col-100 elementor-top-column"
                                                         data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div data-id="51ca7df"
                                                                     class="elementor-element elementor-element-51ca7df elementor-widget elementor-widget-text-editor"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #ffffff;">Ищите отделочную компанию по ремонту квартиры под ключ в Москве, Зеленограде, Мытищах и других городах Подмосковья? – «Наша Бригада» к вашим услугам!</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                                <section data-id="739670d"
                                                                         class="elementor-element elementor-element-739670d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;zigzag&quot;}"
                                                                         data-element_type="section">
                                                                    <div class="elementor-shape elementor-shape-top"
                                                                         data-negative="false">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 1800 5.8"
                                                                             preserveAspectRatio="none">
                                                                            <path class="elementor-shape-fill"
                                                                                  d="M5.4.4l5.4 5.3L16.5.4l5.4 5.3L27.5.4 33 5.7 38.6.4l5.5 5.4h.1L49.9.4l5.4 5.3L60.9.4l5.5 5.3L72 .4l5.5 5.3L83.1.4l5.4 5.3L94.1.4l5.5 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.4 5.3L161 .4l5.4 5.3L172 .4l5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.5 5.3L261 .4l5.4 5.3L272 .4l5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.7-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.4h.2l5.6-5.4 5.5 5.3L361 .4l5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.7-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.6-5.4 5.5 5.3L461 .4l5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1L550 .4l5.4 5.3L561 .4l5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2L650 .4l5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2L750 .4l5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.7-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.4h.2L850 .4l5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.4 5.3 5.7-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.7-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.4 5.3 5.7-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.7-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.7-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.4h.2l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.7-5.4 5.4 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.5 5.4h.1l5.6-5.4 5.5 5.3 5.6-5.3 5.5 5.3 5.6-5.3 5.4 5.3 5.7-5.3 5.4 5.3 5.6-5.3 5.5 5.4V0H-.2v5.8z"></path>
                                                                        </svg>
                                                                    </div>
                                                                    <div class="elementor-container elementor-column-gap-default">
                                                                        <div class="elementor-row">
                                                                            <div data-id="19b1213"
                                                                                 class="elementor-element elementor-element-19b1213 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="3dc7113"
                                                                                             class="elementor-element elementor-element-3dc7113 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p style="text-align: justify;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ffffff;">Заказать выезд замерщика для осмотра объекта и запросить стоимость ремонта квартиры можно по телефону <strong><span
                                                                                                                        style="color: #000000;">8 (985) 127-85-59</span> </strong>или оставив заявку на сайте через форму обратной связи.</span>
                                                                                                    </p>
                                                                                                    <p></p>
                                                                                                    <div class="caldera-grid"
                                                                                                         id="caldera_form_1"
                                                                                                         data-cf-ver="1.6.3"
                                                                                                         data-cf-form-id="CF5aa56c3f7628a">
                                                                                                        <div id="caldera_notices_1"
                                                                                                             data-spinner="https://nashabrigada.ru/wp-admin/images/spinner.gif"></div>
                                                                                                        <form data-instance="1"
                                                                                                              class="CF5aa56c3f7628a caldera_forms_form cfajax-trigger _tisBound"
                                                                                                              method="POST"
                                                                                                              enctype="multipart/form-data"
                                                                                                              id="CF5aa56c3f7628a_1"
                                                                                                              data-form-id="CF5aa56c3f7628a"
                                                                                                              aria-label="Вызов мастера"
                                                                                                              data-target="#caldera_notices_1"
                                                                                                              data-template="#cfajax_CF5aa56c3f7628a-tmpl"
                                                                                                              data-cfajax="CF5aa56c3f7628a"
                                                                                                              data-load-element="_parent"
                                                                                                              data-load-class="cf_processing"
                                                                                                              data-post-disable="0"
                                                                                                              data-action="cf_process_ajax_submit"
                                                                                                              data-request="https://nashabrigada.ru/cf-api/CF5aa56c3f7628a"
                                                                                                              data-hiderows="true">
                                                                                                            <input type="hidden"
                                                                                                                   id="_cf_verify_CF5aa56c3f7628a"
                                                                                                                   name="_cf_verify"
                                                                                                                   value="7ead9b152a"
                                                                                                                   data-nonce-time="1526477175"><input
                                                                                                                    type="hidden"
                                                                                                                    name="_wp_http_referer"
                                                                                                                    value="/"><input
                                                                                                                    type="hidden"
                                                                                                                    name="_cf_frm_id"
                                                                                                                    value="CF5aa56c3f7628a">
                                                                                                            <input type="hidden"
                                                                                                                   name="_cf_frm_ct"
                                                                                                                   value="1">
                                                                                                            <input type="hidden"
                                                                                                                   name="cfajax"
                                                                                                                   value="CF5aa56c3f7628a">
                                                                                                            <input type="hidden"
                                                                                                                   name="_cf_cr_pst"
                                                                                                                   value="2">
                                                                                                            <div class="hide"
                                                                                                                 style="display:none; overflow:hidden;height:0;width:0;">
                                                                                                                <label>Url</label><input
                                                                                                                        type="text"
                                                                                                                        name="url"
                                                                                                                        value=""
                                                                                                                        autocomplete="off">
                                                                                                            </div>
                                                                                                            <div id="CF5aa56c3f7628a_1-row-1"
                                                                                                                 class="row first_row">
                                                                                                                <div class="col-sm-4 first_col">
                                                                                                                    <div data-field-wrapper="fld_8768091"
                                                                                                                         class="form-group"
                                                                                                                         id="fld_8768091_1-wrap">
                                                                                                                        <label id="fld_8768091Label"
                                                                                                                               for="fld_8768091_1"
                                                                                                                               class="control-label">Имя
                                                                                                                            <span aria-hidden="true"
                                                                                                                                  role="presentation"
                                                                                                                                  class="field_required"
                                                                                                                                  style="color:#ee0000;">*</span></label>
                                                                                                                        <div class="">
                                                                                                                            <input required=""
                                                                                                                                   type="text"
                                                                                                                                   data-field="fld_8768091"
                                                                                                                                   class=" form-control"
                                                                                                                                   id="fld_8768091_1"
                                                                                                                                   name="fld_8768091"
                                                                                                                                   value=""
                                                                                                                                   data-type="text"
                                                                                                                                   aria-required="true"
                                                                                                                                   aria-labelledby="fld_8768091Label">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div data-field-wrapper="fld_9970286"
                                                                                                                         class="form-group"
                                                                                                                         id="fld_9970286_1-wrap">
                                                                                                                        <label id="fld_9970286Label"
                                                                                                                               for="fld_9970286_1"
                                                                                                                               class="control-label">Ваш
                                                                                                                            телефон
                                                                                                                            <span aria-hidden="true"
                                                                                                                                  role="presentation"
                                                                                                                                  class="field_required"
                                                                                                                                  style="color:#ee0000;">*</span></label>
                                                                                                                        <div class="">
                                                                                                                            <input required=""
                                                                                                                                   type="text"
                                                                                                                                   data-field="fld_9970286"
                                                                                                                                   class=" form-control"
                                                                                                                                   id="fld_9970286_1"
                                                                                                                                   name="fld_9970286"
                                                                                                                                   value=""
                                                                                                                                   data-type="text"
                                                                                                                                   aria-required="true"
                                                                                                                                   aria-labelledby="fld_9970286Label">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-sm-8 last_col">
                                                                                                                    <div data-field-wrapper="fld_7683514"
                                                                                                                         class="form-group"
                                                                                                                         id="fld_7683514_1-wrap">
                                                                                                                        <label id="fld_7683514Label"
                                                                                                                               for="fld_7683514_1"
                                                                                                                               class="control-label">Тема
                                                                                                                            заявки
                                                                                                                            <span aria-hidden="true"
                                                                                                                                  role="presentation"
                                                                                                                                  class="field_required"
                                                                                                                                  style="color:#ee0000;">*</span></label>
                                                                                                                        <div class="">
                                                                                                                            <textarea
                                                                                                                                    name="fld_7683514"
                                                                                                                                    value=""
                                                                                                                                    data-field="fld_7683514"
                                                                                                                                    class="form-control"
                                                                                                                                    id="fld_7683514_1"
                                                                                                                                    rows="7"
                                                                                                                                    required="required"
                                                                                                                                    aria-labelledby="fld_7683514Label"></textarea>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div data-field-wrapper="fld_7908577"
                                                                                                                         class="form-group"
                                                                                                                         id="fld_7908577_1-wrap">
                                                                                                                        <div class="">
                                                                                                                            <input class="btn btn-default"
                                                                                                                                   type="submit"
                                                                                                                                   name="fld_7908577"
                                                                                                                                   id="fld_7908577_1"
                                                                                                                                   value="Отправить сообщение"
                                                                                                                                   data-field="fld_7908577">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <input class="button_trigger_1"
                                                                                                                           type="hidden"
                                                                                                                           name="fld_7908577"
                                                                                                                           id="fld_7908577_1_btn"
                                                                                                                           value=""
                                                                                                                           data-field="fld_7908577">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div id="CF5aa56c3f7628a_1-row-2"
                                                                                                                 class="row last_row">
                                                                                                                <div class="col-sm-12 single"></div>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>
                                                                                                    <p></p></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-id="0d5e28e"
                                                                                             class="elementor-element elementor-element-0d5e28e elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p style="text-align: center;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif;"><span
                                                                                                                    style="color: #ffffff;">Нажимая кнопку <strong>«Отправить сообщение»</strong>, я даю согласие на</span><span
                                                                                                                    style="color: #000000;"> <a
                                                                                                                        style="color: #000000;"
                                                                                                                        href="//nashabrigada.ru/politika-obrabotki-personalnyh-dannyh/"
                                                                                                                        target="_blank"
                                                                                                                        rel="nofollow noopener">обработку персональных данных</a></span>, <span
                                                                                                                    style="color: #ffffff;">а также принимаю</span> <span
                                                                                                                    style="color: #000000;"><a
                                                                                                                        style="color: #000000;"
                                                                                                                        href="//nashabrigada.ru/polzovatelskoe-soglashenie/"
                                                                                                                        target="_blank"
                                                                                                                        rel="nofollow noopener">пользовательское соглашение</a> <span
                                                                                                                        style="color: #ffffff;">и</span> <a
                                                                                                                        style="color: #000000;"
                                                                                                                        href="//nashabrigada.ru/politika-konfidentsialnosti/"
                                                                                                                        target="_blank"
                                                                                                                        rel="nofollow noopener">политику конфиденциальности</a><span
                                                                                                                        style="color: #ffffff;">.</span></span></span>
                                                                                                    </p></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="116e3f1"
                                                                                 class="elementor-element elementor-element-116e3f1 elementor-column elementor-col-50 elementor-inner-column"
                                                                                 data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div data-id="ffcaa8b"
                                                                                             class="elementor-element elementor-element-ffcaa8b elementor-widget elementor-widget-image"
                                                                                             data-element_type="image.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-image">
                                                                                                    <img width="1160"
                                                                                                         height="722"
                                                                                                         src="https://i2.wp.com/nashabrigada.ru/wp-content/uploads/2018/03/kap-karemont.jpg?fit=1160%2C722&amp;ssl=1"
                                                                                                         class="attachment-large size-large"
                                                                                                         alt=""
                                                                                                         srcset="//i2.wp.com/nashabrigada.ru/wp-content/uploads/2018/03/kap-karemont.jpg?w=1160&amp;ssl=1 1160w, https://i2.wp.com/nashabrigada.ru/wp-content/uploads/2018/03/kap-karemont.jpg?resize=768%2C478&amp;ssl=1 768w, https://i2.wp.com/nashabrigada.ru/wp-content/uploads/2018/03/kap-karemont.jpg?resize=500%2C311&amp;ssl=1 500w, https://i2.wp.com/nashabrigada.ru/wp-content/uploads/2018/03/kap-karemont.jpg?resize=1024%2C637&amp;ssl=1 1024w"
                                                                                                         sizes="(max-width: 640px) 100vw, 640px">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-id="eaccd05"
                                                                                             class="elementor-element elementor-element-eaccd05 elementor-widget elementor-widget-text-editor"
                                                                                             data-element_type="text-editor.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                                    <p style="text-align: center;">
                                                                                                        <span style="font-family: arial, helvetica, sans-serif; color: #ffffff;">Воплотим ваши мечты в реальность и преобразим жилое помещение в уютное, красивое и удобное.</span>
                                                                                                    </p></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <div data-id="4ba2ef8"
                                                                     class="elementor-element elementor-element-4ba2ef8 elementor-widget elementor-widget-spacer"
                                                                     data-element_type="spacer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-spacer">
                                                                            <div class="elementor-spacer-inner"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="1bb4e7e"
                                                                     class="elementor-element elementor-element-1bb4e7e animated animated-slow elementor-invisible elementor-widget elementor-widget-text-editor"
                                                                     data-settings="{&quot;_animation&quot;:&quot;flash&quot;}"
                                                                     data-element_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                            <p style="text-align: center;"><span
                                                                                        style="font-family: arial, helvetica, sans-serif; color: #ffffff;">Выбирайте качество, ключи от которого находятся в наших руках!</span>
                                                                            </p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


{{--+++++++++++++++++++--}}

        @endif

    </div>
</div>
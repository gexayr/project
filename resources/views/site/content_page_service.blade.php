<section>
    <h1 class="hero-title">{{ $page->name }}</h1>

    <div class="container">
        <div class="row">
            <div class="inner-wrapper">
                <h2>{{ $page->name }}</h2>


                @if(isset($page->images))
                    {!! Html::image('img/'.$page->images,  null, array('class'=>'img','style'=>"width: 18rem;")) !!}
                @endif

                <p>
                    {!! $page->text !!}
                </p>

                <div class="col-md-12">
                @if(($page->alias)=='services')
                    @foreach($services as $service)
                    <div class="col-md-12" style="margin-top: 50px">

                        <b>{{$service->name}}</b>
                        <br>
                    </div>

                            <table class="table">
                                <thead>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($prices as $price)
                                    @if($price->service_id == $service->id)
                                        <tr>
                                            <td class="td_1">{{ $price->name }}</td>
                                            <td class="td_2">{{ $price->price }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                    @endforeach
                    <span>✯ Все цены (расценки) в этом прайс листе нужно использовать как ориентировочные, стоимость от которых начинаются данные виды работ.</span>
                @endif
                </div>
            </div>
            {!! link_to(route('home'), 'На главную', array('class'=>'pull-right')) !!}
        </div>
    </div>
</section>

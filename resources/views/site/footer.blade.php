<!--   Footer   -->

{{--++++++++++++++++++++++++++++++++++++++++--}}

<div class="footer footer-content-lists footer-border-accent">
    <div class="footer-content">
        <div class="gridContainer container">
            <div class="row">
                <div class="col-sm-8 flexbox">
                    <div class="row widgets-row">

                        @foreach($menu as $item)
                            @if($item['menu'] != 1 )
                                <?php
                                 $footer_menu[] = $item;
                                ?>
                            @endif
                        @endforeach

                        <?php

                                if(count($footer_menu)%2 != 0){
                                    $lenght = (count($footer_menu)+1)/2;
                                }else{
                                    $lenght = count($footer_menu)/2;
                                }

                        ?>

                        <div class="col-sm-4">
                            <div id="nav_menu-15" class="widget widget_nav_menu"><h4 class="widgettitle">Виды ремонтов</h4>
                                <div>
                                    <ul class="menu">
                                        @foreach($footer_menu as $key=>$value)

                                        @if($key < $lenght )
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page">

                                                    <a target="_blank" href="{{ $value['alias'] }}">{{ $value['title'] }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page">--}}
                                            {{--<a target="_blank" href="//nashabrigada.ru/budzetniy-remont-ekonom-klassa-v-novostrojke/">Эконом--}}
                                                {{--ремонт</a></li>--}}
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page">--}}
                                            {{--<a target="_blank" href="//nashabrigada.ru/kosmeticheskij-remont-kvartir-ceny-stoimost-moskva/">Косметический</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page ">--}}
                                            {{--<a target="_blank" href="//nashabrigada.ru/kapitalnyj-remont-kvartir/">Капитальный</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33949">--}}
                                            {{--<a target="_blank" href="//nashabrigada.ru/skolko-stoit-chernovoj-remont-kvartiry-v-novostrojke/">Черновой ремонт</a>--}}
                                        {{--</li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="widget widget_nav_menu"><h4 class="widgettitle">Стены и потолки</h4>
                                <div class="menu-prochie-container">
                                    <ul id="menu-prochie" class="menu">
                                        @foreach($footer_menu as $key=>$value)

                                            @if($key >= $lenght )
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page">

                                                    <a target="_blank" href="{{ $value['alias'] }}">{{ $value['title'] }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34162">--}}
                                            {{--<a href="//nashabrigada.ru/skolko-stoit-pokraska-sten-v-kvartire-cena-i-stoimost-pokraski/">Покраска стен и потолков</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page ">--}}
                                            {{--<a href="//nashabrigada.ru/hudozhestvennaya-rospis-sten-i-potolkov/">Художественная роспись</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34166">--}}
                                            {{--<a title="Нанесение декоративной штукатурки: цены за м2 работы"--}}
                                               {{--href="//nashabrigada.ru/dekorativnaya-shtukaturka/">Декоративная--}}
                                                {{--штукатурка</a></li>--}}
                                        {{--<li class="menu-item menu-item-type-post_type menu-item-object-page ">--}}
                                            {{--<a href="//nashabrigada.ru/ustanovka-natyazhnyh-potolkov/">Потолок за 24 часа!</a>--}}
                                        {{--</li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                </div>
                <div class="col-sm-4 flexbox text-center middle-xs footer-column-black">
                    <div>
                        <div class="footer-logo space-bottom-small"><h2><span data-type="group" data-dynamic-mod="true">Наш Строй</span>
                            </h2></div>
                        <p class="copyright">©&nbsp;&nbsp;2018&nbsp;Наш Строй.&nbsp;Профессиональный <a
                                    target="_blank" href="/" class="mesmerize-theme-link">ремонт
                                квартиры под ключ</a></p>
                        <div data-type="group" data-dynamic-mod="true" class="footer-social-icons"><a target="_blank"
                                                                                                      class="social-icon"
                                                                                                      href=""> <i
                                        class="fa fa-check-square-o"></i> </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
{{--++++++++++++++++++++++++++++++++++++++++--}}


<footer class="myfooter">
    <div class="container" style="margin-bottom: 40px;">
        <div class="row">
           <div class="col-md-8 col-md-offset-2 text-center">
               <p>Сайт не является публичной офертой и носит информационный характер</p>
           </div>
        </div>

        <div class="text-center"> © 2018. <a href="https://blessedsolutions.org/">Blessedsolutions.org</a></div>
        <hr>

    </div> <!-- ./container -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 34px;">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    {{--==========================--}}

                        <div class="col-md-12">
                            {!! Form::open(['url'=>route('home'), 'method'=>'POST', 'class'=>'form-horizontal']) !!}

                            <div class="form-group">
                                {!! Form::label('name','Имя',['class'=>'col-xs-2 control-label']) !!}
                                <div class="col-xs-8">
                                    {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ваше имя ']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('email','Email',['class'=>'col-xs-2 control-label']) !!}
                                <div class="col-xs-8">
                                    {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>'Ваш Email']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('text','Сообщение',['class'=>'col-xs-2 control-label']) !!}
                                <div class="col-xs-8">
                                    {!! Form::textarea('text',old('text'),['class'=>'form-control']) !!}
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-8">
                                    {!! Form::button('Отправить',['class'=>'save btn btn-primary','type'=>'submit']) !!}
                                </div>
                            </div>

                            {{csrf_field()}}

                            {!! Form::close() !!}
                        </div>
                    {{--==========================--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->

</footer>
<div class="myfooter-bottom">
    <div class="container">
        <a href="#" data-toggle="modal" data-target="#myModal"> <i class="fa fa-home"></i> По вопросам ремонта звоните, пишите: | <i class="fa fa-phone"></i> +7 (985) 127-85-59 | <i class="fa fa-envelope"></i> info@nashstroy.ru</a>

    </div>
</div>
<!--/.footer-bottom-->
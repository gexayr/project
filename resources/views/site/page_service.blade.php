@extends('layouts.site')

@section('header')

    @include('site.header')

@endsection


@section('content')

    @include('site.content_page_service')

@endsection


@section('footer')

    @include('site.footer')

@endsection

<section>
    <h1 class="hero-title">{{ $page->name }}</h1>

    <div class="container">
        <div class="row">
            <div class="inner-wrapper">
                {{--<h2>{{ $page->name }}</h2>--}}


                @if(isset($page->images))
                    {!! Html::image('img/'.$page->images,  null, array('class'=>'img','style'=>"width: 18rem;")) !!}
                @endif

                <p>
                    {!! $page->text !!}
                </p>

                <div class="col-md-12">
                @if(($page->alias)=='contacts')
                        {{--<div class="col-md-12">--}}
                            {{--{!! Form::open(['url'=>route('home'), 'method'=>'POST', 'class'=>'form-horizontal']) !!}--}}

                            {{--<div class="form-group">--}}
                                {{--{!! Form::label('name','Имя',['class'=>'col-xs-2 control-label']) !!}--}}
                                {{--<div class="col-xs-8">--}}
                                    {{--{!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ваше имя ']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--{!! Form::label('email','Email',['class'=>'col-xs-2 control-label']) !!}--}}
                                {{--<div class="col-xs-8">--}}
                                    {{--{!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>'Ваш Email']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--{!! Form::label('text','Сообщение',['class'=>'col-xs-2 control-label']) !!}--}}
                                {{--<div class="col-xs-8">--}}
                                    {{--{!! Form::textarea('text',old('text'),['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}


                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-offset-2 col-md-8">--}}
                                    {{--{!! Form::button('Отправить',['class'=>'save btn btn-primary','type'=>'submit']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--{{csrf_field()}}--}}

                            {{--{!! Form::close() !!}--}}
                        {{--</div>--}}

{{--++++++++++++++++++++++++--}}
    <div id="post-10392" class="post-10392 page type-page status-publish hentry">
        <div>
            <div class="elementor elementor-10392">
                <div class="elementor-inner">
                    <div class="elementor-section-wrap">
                        <section data-id="50cad84"
                                 class="elementor-element elementor-element-50cad84 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                 data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                 data-element_type="section">
                            <div class="elementor-background-overlay"></div>
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div data-id="a734fdc"
                                         class="elementor-element elementor-element-a734fdc elementor-column elementor-col-50 elementor-top-column"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="cd6ccac"
                                                     class="elementor-element elementor-element-cd6ccac elementor-widget elementor-widget-text-editor"
                                                     data-element_type="text-editor.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-text-editor elementor-clearfix"><p
                                                                    style="text-align: center;"><span
                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Просим вас предварительно звонить и договариваться о встрече или отправлять заявки на консультацию на адрес электронной почты. Наши специалисты постоянно заняты на объектах и согласование времени для звонка или встречи очень важно для нас.</span>
                                                            </p></div>
                                                    </div>
                                                </div>
                                                <section data-id="491665a"
                                                         class="elementor-element elementor-element-491665a elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                         data-element_type="section">
                                                    <div class="elementor-container elementor-column-gap-default">
                                                        <div class="elementor-row">
                                                            <div data-id="45f8ebb"
                                                                 class="elementor-element elementor-element-45f8ebb elementor-column elementor-col-50 elementor-inner-column"
                                                                 data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div data-id="bb061f5"
                                                                             class="elementor-element elementor-element-bb061f5 elementor-view-framed elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                             data-element_type="icon-box.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-icon-box-wrapper">
                                                                                    <div class="elementor-icon-box-icon">
                                                                <span class="elementor-icon elementor-animation-"> <i
                                                                            class="fa fa-mobile"
                                                                            aria-hidden="true"></i> </span>
                                                                                    </div>
                                                                                    <div class="elementor-icon-box-content">
                                                                                        <p class="elementor-icon-box-title">
                                                                                            <span>Телефон</span></p>
                                                                                        <p class="elementor-icon-box-description">
                                                                                            8 (985) 127 8559</p></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-id="b3d3480"
                                                                 class="elementor-element elementor-element-b3d3480 elementor-column elementor-col-50 elementor-inner-column"
                                                                 data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div data-id="12f8cbc"
                                                                             class="elementor-element elementor-element-12f8cbc elementor-view-framed elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                             data-element_type="icon-box.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-icon-box-wrapper">
                                                                                    <div class="elementor-icon-box-icon">
                                                                <span class="elementor-icon elementor-animation-"> <i
                                                                            class="fa fa-envelope-o"
                                                                            aria-hidden="true"></i> </span>
                                                                                    </div>
                                                                                    <div class="elementor-icon-box-content">
                                                                                        <p class="elementor-icon-box-title">
                                                                                            <span>Е-mail</span></p>
                                                                                        <p class="elementor-icon-box-description">
                                                                                            info@nashabrigada.ru</p></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section data-id="c48f5d0"
                                                         class="elementor-element elementor-element-c48f5d0 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                         data-element_type="section">
                                                    <div class="elementor-container elementor-column-gap-default">
                                                        <div class="elementor-row">
                                                            <div data-id="88aa014"
                                                                 class="elementor-element elementor-element-88aa014 elementor-column elementor-col-50 elementor-inner-column"
                                                                 data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div data-id="4761351"
                                                                             class="elementor-element elementor-element-4761351 elementor-view-framed elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                             data-element_type="icon-box.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-icon-box-wrapper">
                                                                                    <div class="elementor-icon-box-icon">
                                                                <span class="elementor-icon elementor-animation-"> <i
                                                                            class="fa fa-whatsapp"
                                                                            aria-hidden="true"></i> </span>
                                                                                    </div>
                                                                                    <div class="elementor-icon-box-content">
                                                                                        <p class="elementor-icon-box-title">
                                                                                            <span>WhatsApp</span></p>
                                                                                        <p class="elementor-icon-box-description">
                                                                                            8 (926) 900 1779</p></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-id="5eef48d"
                                                                 class="elementor-element elementor-element-5eef48d elementor-column elementor-col-50 elementor-inner-column"
                                                                 data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div data-id="57c5942"
                                                                             class="elementor-element elementor-element-57c5942 elementor-view-framed elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                             data-element_type="icon-box.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-icon-box-wrapper">
                                                                                    <div class="elementor-icon-box-icon">
                                                                <span class="elementor-icon elementor-animation-"> <i
                                                                            class="fa fa-whatsapp"
                                                                            aria-hidden="true"></i> </span>
                                                                                    </div>
                                                                                    <div class="elementor-icon-box-content">
                                                                                        <p class="elementor-icon-box-title">
                                                                                            <span>WhatsApp</span></p>
                                                                                        <p class="elementor-icon-box-description">
                                                                                            8 (977) 600 0040</p></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section data-id="06b2524"
                                                         class="elementor-element elementor-element-06b2524 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                         data-element_type="section">
                                                    <div class="elementor-container elementor-column-gap-default">
                                                        <div class="elementor-row">
                                                            <div data-id="724536c"
                                                                 class="elementor-element elementor-element-724536c elementor-column elementor-col-50 elementor-inner-column"
                                                                 data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div data-id="f72206f"
                                                                             class="elementor-element elementor-element-f72206f elementor-view-framed elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                             data-element_type="icon-box.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-icon-box-wrapper">
                                                                                    <div class="elementor-icon-box-icon">
                                                                <span class="elementor-icon elementor-animation-"> <i
                                                                            class="fa fa-map-marker"
                                                                            aria-hidden="true"></i> </span>
                                                                                    </div>
                                                                                    <div class="elementor-icon-box-content">
                                                                                        <p class="elementor-icon-box-title">
                                                                                            <span>Адрес</span></p>
                                                                                        <p class="elementor-icon-box-description">
                                                                                            Москва, Фестивальная улица, 13,
                                                                                            корп. 2</p></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-id="a2874d2"
                                                                 class="elementor-element elementor-element-a2874d2 elementor-column elementor-col-50 elementor-inner-column"
                                                                 data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div data-id="f18b146"
                                                                             class="elementor-element elementor-element-f18b146 elementor-view-framed elementor-shape-circle elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                             data-element_type="icon-box.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-icon-box-wrapper">
                                                                                    <div class="elementor-icon-box-icon">
                                                                <span class="elementor-icon elementor-animation-"> <i
                                                                            class="fa fa-clock-o"
                                                                            aria-hidden="true"></i> </span>
                                                                                    </div>
                                                                                    <div class="elementor-icon-box-content">
                                                                                        <p class="elementor-icon-box-title">
                                                                                            <span>Время работы</span></p>
                                                                                        <p class="elementor-icon-box-description">
                                                                                            Без выходных: с 09:00 до
                                                                                            21:00</p></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-id="e9681c4"
                                         class="elementor-element elementor-element-e9681c4 elementor-column elementor-col-50 elementor-top-column"
                                         data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="9da990f"
                                                     class="elementor-element elementor-element-9da990f elementor-widget elementor-widget-text-editor"
                                                     data-element_type="text-editor.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-text-editor elementor-clearfix"><p
                                                                    style="text-align: center;"><span
                                                                        style="font-family: arial, helvetica, sans-serif; color: #000000;">Вы можете подать заявку на звонок нашего оператора в удобное для Вас время. В сообщении ниже укажите время, в которое Вам удобно принять звонок, Ваше имя и вид работ, который Вам необходимо выполнить.</span>
                                                            </p></div>
                                                    </div>
                                                </div>
                                                <section data-id="e643e92"
                                                         class="elementor-element elementor-element-e643e92 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                         data-element_type="section">
                                                    <div class="elementor-container elementor-column-gap-default">
                                                        <div class="elementor-row">
                                                            <div data-id="be0ecb8"
                                                                 class="elementor-element elementor-element-be0ecb8 elementor-column elementor-col-100 elementor-inner-column"
                                                                 data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div data-id="0148ae8"
                                                                             class="elementor-element elementor-element-0148ae8 elementor-widget elementor-widget-text-editor"
                                                                             data-element_type="text-editor.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                    <p></p>
                                                                                    <div class="caldera-grid"
                                                                                         id="caldera_form_1"
                                                                                         data-cf-ver="1.6.3"
                                                                                         data-cf-form-id="CF5aa3d370b796b">
                                                                                        <div id="caldera_notices_1"
                                                                                             data-spinner="https://nashabrigada.ru/wp-admin/images/spinner.gif"></div>

                                                                                        <form class="contact_page" method="POST" action="/" accept-charset="UTF-8">
                                                                                            {{ csrf_field() }}
                                                                                            <div class="row first_row">
                                                                                                <div class="col-sm-12 single">
                                                                                                    <div class=""><span
                                                                                                                style="font-size: 20px; color: #000000;">Ваши данные</span>
                                                                                                        <p>
                                                                                                            <span style="font-size: 15px; color: #000000;">Дайте нам знать, как с вами связаться</span>
                                                                                                        </p>
                                                                                                        <hr>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row ">
                                                                                                <div class="col-sm-4 first_col">
                                                                                                    <div class="form-group" >
                                                                                                        <label class="control-label">Имя
                                                                                                            <span aria-hidden="true" role="presentation" class="field_required" style="color:#ee0000;">*</span>
                                                                                                        </label>
                                                                                                        <div class="">
                                                                                                            <input required="" type="text" class="form-control" name="name" value="" data-type="text" aria-required="true" >
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-sm-8 last_col">
                                                                                                    <div class="form-group">
                                                                                                        <label class="control-label">Ваш номер телефона
                                                                                                            <span aria-hidden="true" role="presentation" class="field_required" style="color:#ee0000;">*</span></label>
                                                                                                        <div class="">
                                                                                                            <input class="form-control" type="text" name="email" value="">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row ">
                                                                                                <div class="col-sm-12 single">
                                                                                                    <div class=""><span style="font-size: 20px; color: #000000;">Как мы можем вам помочь?</span>
                                                                                                        <p>
                                                                                                            <span style="font-size: 15px; color: #000000;">Не стесняйтесь задавать вопрос или просто оставлять комментарий.</span>
                                                                                                        </p>
                                                                                                        <hr>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row ">
                                                                                                <div class="col-sm-12 single">
                                                                                                    <div class="form-group">
                                                                                                        <label class="control-label screen-reader-text sr-only">Опишите, что вам нужно</label>
                                                                                                        <div class="">
                                                                                                            <textarea name="text" value="" class="form-control" rows="7" ></textarea>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row last_row">
                                                                                                <div class="col-sm-12 single">
                                                                                                    <div class="form-group">
                                                                                                        <div class="">
                                                                                                            <input class="btn btn-default" type="submit" value="Отправить сообщение">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>

                                                                                    </div>
                                                                                    <p></p>
                                                                                    </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--++++++++++++++++++++++++--}}


                @endif
                </div>

            </div>
            {{--{!! link_to(route('home'), 'На главную', array('class'=>'pull-right')) !!}--}}
        </div>
    </div>


</section>

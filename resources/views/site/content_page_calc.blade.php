<section>
    <script src="{{ asset('js/calc.js') }}"></script>

    <h1 class="hero-title">Калькулятор</h1>

    <div class="container">
        <div class="row">
            <div class="inner-wrapper">
                <h2>Калькулятор</h2>

                <div class="content calc">


                    <div class="col-md-12">
                            @foreach($services as $service)
                                <div class="service_block service_{{$service->id}}">
                                    <div class="col-md-12 text-center" style="margin-top: 50px">

                                        <span class="service_name"><b>{{$service->name}}</b></span>
                                        <br>
                                    </div>

                                    <table class="table">
                                        <thead>
                                            <th></th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                        @foreach($prices as $price)
                                            @if($price->service_id == $service->id)
                                                <tr class="serv_tr">
                                                    <td data-label="Названия" class="td_0">{{ $price->name }}</td>
                                                    <td data-label="Значения" class="td_1"><div>{!! Form::number('',null,['min'=>'0','class'=>'form-control','onchange'=>'calc.calculate(this)']) !!}</div> </td>
                                                    <td data-label="Цена" class="td_2">{{ $price->price }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <b><div class="result-block">Итого по разделу "<span></span>" : <label></label> руб.</div></b>
                                </div>
                            @endforeach
                    </div>
                    <div class="col-md-12 text-right" id="result"><h1></h1></div>


                    <div class="form-group">
                        <div class="">
                            {!! Form::button('Заказать',['class'=>'save btn btn-primary','data-toggle'=>'collapse', 'data-target'=>'#form']) !!}
                        </div>
                    </div>

                    {{--==========================--}}

                        <div id="form" class="collapse form-group">
                            <div class="col-md-12">
                                {!! Form::open(['url'=>route('home'), 'method'=>'POST', 'class'=>'form-horizontal']) !!}

                                <div class="form-group">
                                    {!! Form::label('name','Имя',['class'=>'col-xs-2 control-label']) !!}
                                    <div class="col-xs-8">
                                        {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ваше имя ']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('email','Email',['class'=>'col-xs-2 control-label']) !!}
                                    <div class="col-xs-8">
                                        {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>'Ваш Email']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('text','Сообщение',['class'=>'col-xs-2 control-label']) !!}
                                    <div class="col-xs-8">
                                        {!! Form::textarea('text',old('text'),['class'=>'form-control']) !!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-8">
                                        {!! Form::button('Отправить',['class'=>'save btn btn-primary','type'=>'submit']) !!}
                                    </div>
                                </div>

                                {{csrf_field()}}

                                {!! Form::close() !!}
                            </div>
                        </div>
                    {{--==========================--}}
{{--++++++++++++++++++++++++++++++--}}

{{--++++++++++++++++++++++++++++++--}}

                </div>
            </div>
            {!! link_to(route('home'), 'На главную', array('class'=>'pull-right')) !!}
        </div>
    </div>
</section>

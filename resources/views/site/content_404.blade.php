    <style>
        body{
            background-color: #000;
            color: #fff;
        }
        .background {
            /*-webkit-filter: brightness(0.8);*/
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: url(https://assets-9gag-fun.9cache.com/s/fab0aa49/c7a54d28aa8f404f8d774f61ed8d9dc9174d3639/static/dist/web6/img/404/bg.gif) center center no-repeat;
            background-size: cover;
            z-index: -1;
            opacity: 0.2;
        }

        .message {
            text-align: center;
            position: absolute;
            width: 100%;
            top: 50%;
            transform: translateY(-50%);
        }

        .message h1 {
            font-size: 144px;
            font-weight: 100;
            margin-bottom: 16px;
        }

        .message h2 {
            margin-bottom: 24px;
        }

        .message h3 {
            font-weight: bold;
            font-size: 16px;
            margin-bottom: 32px;
        }

        .btn-download {
            display: inline-block;
            line-height: 20px;
            padding: 12px 24px 12px 48px;
            color: #fff;
            font-weight: bold;
            border-radius: 2px;
        }
        a{
            color: #0f74a8;
        }
    </style>
    <div class="background">

    </div>
    <div class="message">
        <h1>404</h1>
        <h2>Нет такой страницы.</h2>
        {{--<h3>Find what you're looking for with our app.</h3>--}}
        <a href="/">На главную</a>
    </div>

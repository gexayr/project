<header>

    <div class="top-container">
        <div class="col-md-8 col-md-offset-2">

            <a href="#" class="" data-toggle="modal" data-target="#myModal" style="font-size: 14.5px; position: relative; top: 3px;"><i class="fa fa-envelope"></i> Написать нам</a>
            {{--<div class="header-top-bar-area area-left">--}}
                <div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i class="fa fa-map-o"></i> <span>Работаем по всей Москве и области!</span>
                </div>
                <div class="top-bar-field calc_div" data-type="group" data-dynamic-mod="true">
                    {{--<i class="fa fa-eercast"></i>--}}
                    <i class="fa fa-calculator"></i>
                    <a href="{{ route('calc') }}" class="pull-left"  style="font-size: 14.5px; margin-top: -2px;"> Калькулятор</a></div>
            {{--</div>--}}
            {{--<div class="header-top-bar-area area-right">--}}
                <div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i
                            class="fa fa-volume-control-phone"></i> <span>+7 (985) 127 8559</span></div>
                <div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i class="fa fa-clock-o"></i>
                    <span>Ежедневно с 09:00-21:00</span></div>
                {{--<div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i class="fa fa-envelope-o"></i>--}}
                    {{--<span>info@nashabrigada.ru</span></div>--}}
            {{--</div>--}}
        </div>

        {{--<div class="header-top-bar-inner ">--}}
            {{--<div class="header-top-bar-area area-left">--}}
                {{--<div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i class="fa fa-map-o"></i> <span>Работаем по всей Москве и области!</span>--}}
                {{--</div>--}}
                {{--<div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i class="fa fa-eercast"></i>--}}
                    {{--<span>С нами всегда комфорт!</span></div>--}}
            {{--</div>--}}
            {{--<div class="header-top-bar-area area-right">--}}
                {{--<div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i--}}
                            {{--class="fa fa-volume-control-phone"></i> <span>+7 (985) 127 8559</span></div>--}}
                {{--<div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i class="fa fa-clock-o"></i>--}}
                    {{--<span>Ежедневно с 09:00-21:00</span></div>--}}
                {{--<div class="top-bar-field" data-type="group" data-dynamic-mod="true"><i class="fa fa-envelope-o"></i>--}}
                    {{--<span>info@nashabrigada.ru</span></div>--}}
            {{--</div>--}}
        {{--</div>--}}

    </div>
    <div class="header top" id="myHeader">
    <div class="container header inner">
        <nav class="navbar">
            <div class="container">

                @if(isset($menu))


                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="/"><span>Наш </span>Строй</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right masthead-nav">
                        @foreach($menu as $item)
                            @if($item['menu'] == 1 )
                                <li><a href="{{ $item['alias'] }}">{{ $item['title'] }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>

                @endif
            </div>
        </nav>
    </div>
    </div>

</header>
{{--==========================--}}

{{--++++++++++++++++++++++++++++++=--}}
<div class="header-wrapper">
    <div class="header color-overlay custom-mobile-image">
        <div class="inner-header-description gridContainer">
            <div class="row header-description-row">
                <div class="col-xs-8 col-xs-offset-2">
                    {{--<h1 class="hero-title"> Укладка паркетной доски любой сложности</h1>--}}
                 </div>
            </div>
        </div>
    </div>
</div>
{{--==========================--}}
@if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
@endif




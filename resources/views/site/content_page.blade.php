<section>
    <h1 class="hero-title">{{ $page->name }}</h1>

    <div class="container">
        <div class="row">
            <div class="inner-wrapper">
                <h2>{{ $page->name }}</h2>


                @if(isset($page->images))
                    {!! Html::image('img/'.$page->images,  null, array('class'=>'img','style'=>"width: 18rem;")) !!}
                @endif

                <p>
                    {!! $page->text !!}
                </p>

            </div>
            {!! link_to(route('home'), 'На главную', array('class'=>'pull-right')) !!}
        </div>
    </div>
</section>
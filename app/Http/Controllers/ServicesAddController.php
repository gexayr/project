<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Validator;

class ServicesAddController extends Controller
{
    public function execute(Request $request)
    {

        if($request->isMethod('post')){
//            $input = $request->all();
            $input = $request->except('_token');

            $validator = Validator:: make($input,[
                'name'=>'required|max:255',
            ]);


//            for ($i=0; $i<count($service_arr); $i++){
//                $service = new Service;
//                $service->name = $service_arr[$i];
//
//                $service->save();
//            }

//            $service->unguard();
//            $service->fill($input);
            $service = new Service;
            $service->name = $input['name'];



            if($service->save()){

                return redirect('admin')->with('status','Service added');
            }
        }

        if(view()->exists('admin.services_add')){
            $data = [
                'title'=>'New Service',
            ];

            return view('admin.services_add',$data);
        }
    }
}

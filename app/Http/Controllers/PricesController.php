<?php

namespace App\Http\Controllers;

use App\Price;
use Illuminate\Http\Request;

class PricesController extends Controller
{
    public function execute()
    {
        if(view()->exists('admin.prices')){
            $prices = Price::all();
            $data = [
                'title'=>'Prices',
                'prices'=>$prices,
            ];

            return view('admin.prices',$data);
        }
    }
}

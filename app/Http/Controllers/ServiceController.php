<?php

namespace App\Http\Controllers;

use App\Page;
use App\Price;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function execute()
    {

        if (view()->exists('site.page_service')) {
            $pages = Page::all();
            $prices = Price::all();
            $services = Service::all();
            $menu = array();
            foreach ($pages as $page){
                $item = array('title'=>$page->name, 'alias'=>$page->alias, 'menu'=>$page->menu);
                array_push($menu,$item);
                if($page->alias == 'services'){
                    $this_page = $page;
                }
            }
            $data=[
                'title'=>'Services',
                'page'=>$this_page,
                'menu'=>$menu,
                'services'=>$services,
                'prices'=>$prices,
            ];
            return view('site.page_service',$data);
        }
    }
}

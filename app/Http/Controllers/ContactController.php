<?php

namespace App\Http\Controllers;


use App\Page;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function execute()
    {
        if (view()->exists('site.page_contact')) {
            $pages = Page::all();
            $menu = array();
            foreach ($pages as $page){
                $item = array('title'=>$page->name, 'alias'=>$page->alias, 'menu'=>$page->menu);
                array_push($menu,$item);
                if($page->alias == 'contacts'){
                    $this_page = $page;
                }
            }
//            dd($pages);

            $data=[
                'title'=>'Contacts',
                'page'=>$this_page,
                'menu'=>$menu,
            ];
            return view('site.page_contact',$data);
        }
    }
}

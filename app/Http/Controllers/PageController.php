<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function execute($alias)
    {

        if (view()->exists('site.page')) {
            $pages = Page::all();
            $menu = array();
            foreach ($pages as $page){
                $item = array('title'=>$page->name, 'alias'=>$page->alias, 'menu'=>$page->menu);
                array_push($menu,$item);
            }
            $page = Page::where('alias',strip_tags($alias))->first();
            if($page==null){
                return view('site.404',array( 'menu'=>$menu));
            }
            $data=[
                'title'=>$page->name,
                'page'=>$page,
                'menu'=>$menu,
            ];
            return view('site.page',$data);
        }else{
            return view('site.404');
        }
    }
}

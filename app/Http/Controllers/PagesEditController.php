<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Validator;

class PagesEditController extends Controller
{
    //
//    public function execute(Page $page, Request $request)
    public function execute($id, Request $request)
    {
        $page = Page::find($id);

        if($request->isMethod('delete')){
            $page->delete();
            return redirect('admin')->with('status','Page removed');
        }

        if($request->isMethod('post')){


        if($request->menu == null){
            $request->menu = 0;
        }


            $input = $request->except('_token');
            $validator = Validator:: make($input,[
                'name'=>'required|max:255',
                'alias'=>'required|unique:pages,alias,'.$input['id'].'|max:255',
                'text'=>'required',
            ]);

            if($validator->fails()){
                return redirect()
                    ->route('pagesEdit',['page'=>$input['id']])
                    ->withErrors($validator);
            }

            if($request->hasFile('images')){
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();

                $file->move(public_path().'/img', $input['images']);

            }else{
                $input['images'] = $input['old_images'];
            }

            unset( $input['old_images']);
            $input['menu'] = $request->menu;
            $page->fill($input);

            if($page->update()){
                return redirect('admin')->with('status','Page edited');
            }
        }

        $old = $page->toArray();
        if(view()->exists('admin.pages_edit')){
            $data = [
                'title'=>'Edit page | '.$old['name'],
                'data'=>$old,
            ];
            return view('admin.pages_edit',$data);
        }
    }
}

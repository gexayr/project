<?php

namespace App\Http\Controllers;

use App\Page;
use App\Price;
use App\Service;
use Illuminate\Http\Request;

class CalcController extends Controller
{
    public function execute()
    {
        if (view()->exists('site.page_calc')) {
            $pages = Page::all();
            $menu = array();
            $prices = Price::all();
            $services = Service::all();

            foreach ($pages as $page){
                $item = array('title'=>$page->name, 'alias'=>$page->alias, 'menu'=>$page->menu);
                array_push($menu,$item);
                if($page->alias == 'contacts'){
                    $this_page = $page;
                }
            }
//            dd($pages);

            $data=[
                'title'=>'Contacts',
//                'page'=>$this_page,
                'services'=>$services,
                'prices'=>$prices,
                'menu'=>$menu,
            ];
            return view('site.page_calc',$data);
        }
    }
//return view('site.page_calc');
//
}

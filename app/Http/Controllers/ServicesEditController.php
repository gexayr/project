<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Validator;


class ServicesEditController extends Controller
{
    public function execute($id, Request $request)
    {
        $service = Service::find($id);


        if($request->isMethod('delete')){
            $service->delete();
            return redirect('admin')->with('status','Service removed');
        }

        if($request->isMethod('post')){
            $input = $request->except('_token');
            $validator = Validator:: make($input,[
                'name'=>'required|max:255',
            ]);

//            $service->fill($input);

            $service->name = $input['name'];

            if($service->update()){
                return redirect('admin')->with('status','Service edited');
            }
        }

        $old = $service->toArray();
        if(view()->exists('admin.services_edit')){
            $data = [
                'title'=>'Edit service | '.$old['name'],
                'data'=>$old,
            ];
            return view('admin.services_edit',$data);
        }
    }
}

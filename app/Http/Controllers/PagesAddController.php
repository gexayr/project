<?php

namespace App\Http\Controllers;

//use Dotenv\Validator;
use App\Page;
use Illuminate\Http\Request;
use Validator;

class PagesAddController extends Controller
{
    public function execute(Request $request)
    {

        if($request->isMethod('post')){
//            $input = $request->all();


            if($request->menu == null){
                $request->menu = 0;
            }


            $input = $request->except('_token');

            $validator = Validator:: make($input,[
                'name'=>'required|max:255',
                'alias'=>'required|unique:pages|max:255',
                'text'=>'required',
            ]);

            if($validator->fails()){
                return redirect()->route('pagesAdd')->withErrors($validator)->withInput();
            }

            if($request->hasFile('images')){
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();

                $file->move(public_path().'/img', $input['images']);

            }

//            for ($i=100; $i<110; $i++){
//                $page = new Page;
//                $page->name = 'name_'.$i;
//                $page->menu = 0;
//                $page->alias = 'url_'.$i;
//                $page->text = $i.' --  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam corporis cum enim hic, laborum molestias neque odio, praesentium quae quas quia quod sapiente veniam voluptas?';
//
//                $page->save();
//            }


            $input['menu'] = $request->menu;
            $page = new Page;
//            $page->unguard();
            $page->fill($input);



            if($page->save()){

                return redirect('admin')->with('status','Page added');
            }
        }

        if(view()->exists('admin.pages_add')){
            $data = [
                'title'=>'New Page',
            ];

            return view('admin.pages_add',$data);
        }
    }
}

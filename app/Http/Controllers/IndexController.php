<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Price;
use App\Service;
use DB;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    public function execute(Request $request)
    {

        if($request->isMethod('post')){

            $messages = [
                'required'=>'error :attribute',
                'email'=>'error | email | :attribute',
            ];

            $this->validate($request, [
                'name'=>'required|max:255',
                'email'=>'required|email',
                'text'=>'required',

            ], $messages);

            $data = $request->all();

            // mail

//            $result = Mail::send('site.email', ['data'=>$data], function (){
//
//            });


            $input = $request->except('_token');
            $to = "inchka4ka@gmail.com";
            $subject = "Заказ";
            $txt = "Имя:".$input['name']." Номер:".$messages['email']." ::::".$messages['text'];
            $headers = "From: webmaster@example.com" . "\r\n" .
                "CC: bs-team@example.com";

            if(mail($to,$subject,$txt,$headers)){
                return redirect()->route('home')->with('status', 'Email is send');
            }
//
//            if($data){
//                return redirect()->route('home')->with('status', 'Email is send');
//            }
        }





        $pages = Page::all();
        $prices = Price::get(array('name','service_id','price'));
        $services = Service::where('id','<',20)->get();

        $tags = Price::all()->groupBy('service_id');

//        dd($tags);
        $menu = array();
        foreach ($pages as $page){
            $item = array('title'=>$page->name, 'alias'=>$page->alias, 'menu'=>$page->menu);
            array_push($menu,$item);
        }
//
//        $item = array('title'=>'Services','alias'=>'services');
//        array_push($menu,$item);
//
//        $item = array('title'=>'Projects','alias'=>'projects');
//        array_push($menu,$item);
//
//        $item = array('title'=>'Contacts','alias'=>'contacts');
//        array_push($menu,$item);

        return view('site.index', array(
            'menu' => $menu,
            'pages' => $pages,
            'prices' => $prices,
            'services' => $services,
            'tags' => $tags,
        ));
    }
}

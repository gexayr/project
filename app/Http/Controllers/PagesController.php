<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function execute()
    {
        if(view()->exists('admin.pages')){
            $pages = Page::all();
            $data = [
                'title'=>'Pages',
                'pages'=>$pages,
            ];

            return view('admin.pages',$data);
        }
    }

    public function regulation()
    {

        if(view()->exists('site.regulation')){
            $pages = Page::all();
            $menu = array();
            foreach ($pages as $page){
                $item = array('title'=>$page->name, 'alias'=>$page->alias, 'menu'=>$page->menu);
                array_push($menu,$item);
            }
            $data = [
                'title'=>'Regulation',
                'pages'=>$pages,
                'menu'=>$menu,
            ];

            return view('site.regulation',$data);
        }
    }
}
